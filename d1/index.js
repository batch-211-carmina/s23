console.log("Hello World!");


//OBJECTS
/*
	- An Object is a data type that is used to represent real world objects
	- It is a collection of related data and/or functionalities
	- Information stored in objects are represented in a "key:value" pair

	'key' - 'property' of an object
	'value' - actual data to be stored

	- Different data type may be stored in an object's property creating complex data structures
*/

	//There are two ways in creating objects in JS
		//1. Object Literal Notation 
			//(let object = {}) **curly braces**

		//2. Object Constructor Notation
			//Object Instantiation (let object = new Object()) 



	//Object Literal Notation
	/*
		- creating objects using initializer/literal notation
		- camelCase

		SYNTAX:
			let objectName = {
				keyA : valueA,
				keyB : valueB
			};
	*/

		let cellphone = {
			name: "Nokia 3210",
			manufactureDate: 1999
		};

		console.log("Result from creating objects using literal notation:");
		console.log(cellphone);

		//MINI ACTIVITY
		let cellphone2 = {
			name: "Samsung Galaxy S22",
			manufactureDate: 2022
		};

		console.log("Result from creating objects using literal notation:");
		console.log(cellphone2);


		let ninja = {
			name: "Naruto Uzumaki",
			village: "Konoha",
			children: ["Boruto, Himawari"]
		};

		console.log(ninja);



	//Object Constructor Notation
	/*
		- Creating Objects using a constructor function
			- Creates a reusable 'function' to create several objects that have the same data structure

			SYNTAX:

				function objectName(keyA,keyB) {
					this.keyA = keyA;
					this.keyB = keyB;
				};
	*/

		//'this' keyword refers to the properties within the object
			//it allows the assignment of new object's properties
			//by associating them with values received from the constructor function's parameter (blueprint)

		function Laptop(name,manufactureDate){
			this.name = name;
			this.manufactureDate = manufactureDate;
		}

		//create an instance object using the Laptop constructor
		let laptop = new Laptop("Lenovo", 2008);
		console.log("Result from creating objects using object constructor");
		console.log(laptop);

		//the "new" operator creates an instance of an object (new object)
		let myLaptop = new Laptop("MacBook Air", 2020);
		console.log("Result from creating objects using object constructor");
		console.log(myLaptop);


		//MINI ACTIVITY
		let newLaptop = new Laptop ('Dell', '2021'); //string or number is ok
		let newLaptop1 = new Laptop ('HP', 2022);
		let newLaptop2 = new Laptop ('Asus', 2019);
		console.log("Result from creating objects using object constructor");
		console.log(newLaptop);
		console.log(newLaptop1);
		console.log(newLaptop2);


		let oldLaptop = Laptop("Portal R2E CCMC", 1980);  //without 'new'
		console.log("Result from creating objects using object constructor");
		console.log(oldLaptop);  //undefined


	//Create Empty Objects
		let computer = {};
		let myComputer = new Object();

		console.log(computer);
		console.log(myComputer);


	//Accessing Object Properties
		
		//1. Using dot notation (recommended to use)
			console.log("Result from dot notation: " + myLaptop.name);

		//2. Using square bracket notation
			console.log("Result from square bracket notation: " + myLaptop["name"]);


	//Accessing array of objects
	/*
		- Accessing object properties using the square bracket notation and array indexes can cause confusion
	*/

		let arrayObj = [laptop,myLaptop];
		//may be confused for accessing array indexes
		console.log(arrayObj[0]["name"]);

		//this tells us that array[0] is an object by using the dot notation
		console.log(arrayObj[0].name);


	//Initializing/Adding/Deleting/Reassigning Object Properties

		//Create an object using object literals

		let car = {};
		console.log("Current value of car object: ");
		console.log(car);

		//Initializing/adding object properties
		car.name = "Honda Civic";
		console.log("Result from adding properties using dot notation: ");
		console.log(car);  //{name: "Honda Civic"};


		//Initializing/adding object properties using bracket notation (not recommended)
		console.log("Result from adding using square bracket notation:");
		car["manufacture date"] = 2019;
		console.log(car);


		//Deleting object properties
		delete car["manufacture date"];  //deleted a property
		console.log("Result from deleting properties:");
		console.log(car);


		//Reassigning object properties
		car["manufactureDate"] = 2019;
		
		car.name = "Toyota Vios";
		console.log("Result from reassigning properties:");
		console.log(car);


	//OBJECT Methods
		// - A method is a function which is a property of an object
	

		let person = {
			name: "John",
			talk: function(){
				console.log("Hello my name is " + this.name);
			}
		};

		console.log(person);
		console.log("Result from object methods: ");
		person.talk();  //or can invoke in the console browser

		person.walk = function(steps){  //can put parameter and put args when invoking --> person.walk(50)
			console.log(this.name + " walked " + steps + " steps forward.");
		}

		console.log(person);
		person.walk(50);

		//Methods are useful for creating reusable functions that perform tasks related to objects

		let friend = {
			firstName: "Joe",
			lastName: "Smith",
			address: {
				city: "Austin",
				country: "Texas"
			},
			emails: ["joe@mail.com", "joesmith@mail.xyz"],
			introduce: function(){
				console.log("Hello my name is " + this.firstName + " " + this.lastName + " " + "I live in " + this.address.city + ", " + this.address.country);
			}
		};

		friend.introduce();


		//Real World Application of Objects
		/*
			Scenario:
			1. We would like to create a game that would have several Pokemon interact with each other
			2. Every Pokemon would have the same set of stats, properties, and function

			Stats:
			name:
			level:
			health: level * 2
			attack: level
		*/

		//Create an object constructor to lessen the process in creating the pokemon

		//In naming convention the first letter should Capital
		function Pokemon(name, level){

			//properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				//Mini Activity
				//If the target health is less than or equal to 0 we will invoke the faint(method), otherwise printout the pokemon's new health
				//example if health is not less than 0
					//rattata's health is now reduced to 5
				//reduces the target object's health property by subracting and reassigning it's value based on the pokemon's attack

				// target.health = target.health - this.attack;
				target.health -= this.attack;

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health <= 0) {
					target.faint();
				}
				
			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}
		};

		let pikachu = new Pokemon("Pikachu", 88);
		console.log(pikachu);

		let rattata = new Pokemon("Ratata", 10);
		console.log(rattata);

		//to invoke the tackle function in the console browser 
			//rattata.tackle(pikachu);
			//pikachu.tackle(rattata);
