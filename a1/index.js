// console.log("Hello World!");

let trainer = {
	name : 'Ash Ketchum',
	age: 18,
	pokemon: ['Pikachu', 'Squirtle' ,'Charmander' , 'Jigglypuff'],
	friends: {
		hoen: ['May', 'Max'],
		kanto: ['Misty', 'Brock']
	},
	talk: function(){
			console.log(this.pokemon[0] + "! I choose you!");
		}
};

	console.log(trainer);

	//dot notation
	console.log("Result of dot notation: ");
	console.log(trainer.name);

	//bracket notation
	console.log("Result of dot notation: ");
	console.log(trainer['pokemon']);

	console.log("Result of talk method: ");
	trainer.talk(); 


	function Pokemon(name, level){

			//properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level * 1.5;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				target.health -= this.attack;

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health <= 0) {
					target.faint();
				}
				
			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}
	
		};

		let pikachu = new Pokemon("Pikachu", 100);
		console.log(pikachu);

		let squirtle = new Pokemon("Squirtle", 50);
		console.log(squirtle);

		let charmander = new Pokemon("Charmander", 85);
		console.log(charmander);

		let jigglypuff = new Pokemon("Jigglypuff", 125);
		console.log(jigglypuff);